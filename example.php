<?php

use Cleverreach\Receiver;
use Cleverreach\Rest;

require 'cleverreach/vendor/autoload.php';

/**
 * Aufbau der Verbindung zu Cleverreach
 */
$rest = new Rest("https://rest.cleverreach.com/v2");

/**
 * Logindaten
 */
$loginData = array(
    'client_id' => '',
    'login' => '',
    'password' => ''
);

$token = $rest->post('/login', $loginData);
$rest->setAuthMode('bearer', $token);

$receiver = new Receiver($rest);
$receiver->setGroup(515380);
$receiver->setForm(163825);
$receiver->setOptIn(true);
$receiver->add('christian@egol.de', array('city' => 'Marne', 'zip' => 25709));
$receiver->save();
