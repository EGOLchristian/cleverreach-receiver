<?php 

namespace Cleverreach;

use Carbon\Carbon;

/**
* Receiver Class
*/
class Receiver
{
    protected $form = 0;
    protected $group = 0;
    protected $sendOptIn = false;
    protected $receivers = array();
    protected $connection;

    function __construct(Rest $connection)
    {
        $this->connection = $connection;
    }

    public function all()
    {
        return $this->connection->get('/groups/' . $this->group . '/receivers');
    }

    public function setForm($form)
    {
        $this->form = $form;
    }

    public function setGroup($group)
    {
        $this->group = $group;
    }

    public function setOptIn($status)
    {
        $this->sendOptIn = $status ? true : false;
    }

    public function add($email, $attributes, $source = 'RestFul API')
    {
        $this->receivers[] = array(
            'email' => $email,
            'registered' => time(),
            'activated' => $this->sendOptIn ? 0 : time(),
            'source' => $source,
            'attributes' => $attributes
        );
    }

    public function save()
    {
        foreach ($this->receivers as $receiver) {
            $result = $this->connection->post('/groups/' . $this->group . '/receivers', $receiver);

            if ($this->sendOptIn) {
                $this->sendOptIn($receiver);
            }
        }
    }

    protected function sendOptIn($receiver)
    {
        $this->connection->post('/forms.json/' . $this->form . '/send/activate', array(
            'email' => $receiver['email'],
            'doidata' => array(
                "user_ip"    => $_SERVER["REMOTE_ADDR"],
                "referer"    => $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],
                "user_agent" => $_SERVER["HTTP_USER_AGENT"]
            )
        ));
    }
}